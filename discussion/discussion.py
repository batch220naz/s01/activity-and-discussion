import math

# ctrl + / - comment in python
# instead of double /, python uses pound sign (#): 1 liner
"""
although we have keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""

# print is similar to console.log in node/JS it lets us print message in the console/terminal
print("Hello World")

# [SECTION] Variables
# we dont have to use keywords in python to set our variables 
# snake case is the convention terms of naming thr variables in python. It uses underscore(_) in between words and uses lowercasing
age=35
middle_initial="C"
print(age)
print(middle_initial)
# declaring and assigning values to multiple variables simultaneously is possible in python
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name1)
print(name2)
print(name3)
print(name4) 

# [SECTION] Data Types
# string
full_name = "John Doe"
secret_code = 'Pa$$word'

# number
num_of_days = 365 # integer
pi_approx = 3.1416 # float
complex_num = 1+5j # complex
# print(complex_num)

# boolean
is_learning = True
is_difficult = False

print("Hi! My name is " + full_name)

# [SECTION] f strings
# Similar to JS template literals
print(f"Hi! My name is {full_name} and my age is {age}.")

# Typecasting
# is python's way of converting one data type into another since it does not have type coercion like in JS
# print("Hi! My age is " + age) - would result in an error

# form int to string
print("Hi! My age is " + str(age))

# from string to int
print(age + int("9876"))

# integer is different from float in python
print(age + float(98.87))

# [SECTION] Operators
print(2 + 10) # add
print(2 - 10) # subtraction
print(2 * 10) # multiplication
print(2 / 10) # division
print(2 % 10) # remainder
print(2 ** 10) # exponent
print(math.sqrt(81)) # this is possible if we import built in python


# [SECTION] Assignment Operators
num1 = 3
print(num1)
num1 = num1 + 4 # num1 += num1
print(num1)
num1 = num1 - 4 # num1 -= num1
print(num1)
num1 = num1 * 4 # num1 *= num1
print(num1)
num1 = num1 / 4  # num1 /= num1
print(num1)
num1 = num1 % 4  # num1 %= num1
print(num1)


# [SECTION] Comparison Operators
# Returns boolean value to compare values
print(1 == 1)
print(1 != "1")
print(1 < 1)
print(1 > 1)
print(1 <= 1)
print(1 >= 1)


# [SECTION] Logical Operators
print(True and False)
print(True or False)
print(not False)












